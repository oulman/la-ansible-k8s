## ls-ansible-k8s

### Introduction

This ansible playbook automates the base configuration and package installation for the 
Linux Academy Kubernetes Quickstart course.

### Run

```bash
ansible-playbook -i inventories/la deploy.yml --ask-sudo-pass -u cloud_user
```